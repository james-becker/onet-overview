import axios from 'axios'
import { globals } from '@/constants.js'

export default {
  fetchOccupationByArea (data) {
    // return axios.get(`${globals.API_URL}/occupations/${occupation}/${region}`)
    const headers = {
      'Content-Type': 'application/json'
    }
    return axios.post(
      `${globals.API_URL}/occupations`,
      data,
      { headers }
    )
  }
}
